from crypt import methods
import json
from flask import Flask, render_template, request, jsonify
from ui_logger import ui_log, render_logs
import back_end as be

app = Flask(__name__, static_url_path='',
            static_folder='static',
            template_folder='templates')


@app.route("/", methods=['GET', 'POST'])
def main_route():
    global be
    if request.method == "GET":
        try:
            n = request.args.get("n")
            e = request.args.get("e")
            c = request.args.get("c")
            if not (n and e and c):
                return render_template("main.html")
            n = int(n)
            e = int(e)
            c = int(c)

            check_str = request.args.get("primality")
            check_algo = be.algo_str_to_num(check_str)

            factor_str = request.args.get("factorization")
            factor_algo = be.algo_str_to_num(factor_str)

            ui_log("primality algorithm: " + check_str)
            ui_log("factor algorithm: " + factor_str)

            # prime number
            ui_log("Checking for prime-ness ...")
            if be.is_prime(n, check_algo) == 1:
                text = "{0} is a prime number"
                ui_log(text.format(n))

                p = n
                q = 0

            else:   # composite
                text = "{0} is a composite number"
                ui_log(text.format(n))

                ui_log("Factoring ...")
                p = be.factor_number(n, factor_algo)

                # successfully factor
                if p != 1:
                    q = int(n // p)
                    text = "Factor complete. {0} = {1} * {2}"
                    ui_log(text.format(n, p, q))
                else:
                    ui_log("Fail to factor.")
                    raise Exception("Fail to factor")
                    pass  # raise exception

            ui_log("Calculating phi ...")
            phi = be.calculate_phi(p, q)

            ui_log("Calculating d ...")
            d = be.calculate_d(e, phi)

            ui_log("Decrypting ...")
            m = be.decrypt(c, d, n)
            return render_template('main.html', d=d, m=m, p=p, q=q, phi=phi, n=n, c=c, e=e, factor_str=factor_str, check_str=check_str, logs=render_logs())
        except Exception as e:
            return render_template('main.html', n=n, c=c, e=e, factor_str=factor_str, check_str=check_str, logs=render_logs())

    if request.method == "POST":

        return render_template('main.html')


@app.route('/hi/<username>')
def greet(username=None):
    return render_template('main.html', username=username)
