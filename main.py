
from app import app
import os
from dotenv import dotenv_values
config = dotenv_values(".env")

if __name__ == "__main__":
    app.config['DEBUG'] = config["DEBUG"]
    app.run()    