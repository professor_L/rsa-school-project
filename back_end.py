import math_util as mu

def algo_str_to_num(name: str) -> int:
    '''
    Map algorithm name to number
    '''

    if name == "bruteforce" or name == "pollard":
        return 0
    elif name == "rabinmiller" or name == "lenstra":
        return 1
    elif name == "pollard_rho":
        return 2
    else:
        return -1

'''
check prime algo number
0: brute force
1: miller_rabin
'''
def is_prime(n: int, algo) -> bool:
    '''
    n: integer to be checked
    algo: algorithm used
    return: True if prime, False if composite
    '''
    n = int(n)

    if algo == 0:
        return mu.is_prime_brute_force(n)
    elif algo == 1:
        return mu.is_prime_miller_rabin(n)
    
    return False

'''
factorization algo number
0: pollard
1: lenstra
2: pollard_rho
'''
def factor_number(n: int, algo) -> int:
    '''
    n: number to be factored
    algo: algorithm to use
    return: > 1 if succeeds, = 1 if fails
    '''
    n = int(n)
    
    if algo == 0:
        return mu.factor_pollard(n)
    elif algo == 1:
        return mu.factor_lenstra(n)
    elif algo == 2:
        return mu.factor_pollard_rho(n)

    return 1

def calculate_phi(p: int,  q:int) -> int:
    '''
    2 cases:
    - if N is composite, return (p - 1) * (q - 1)
    - if N is prime, return (p - 1)
    '''

    ret = int(p-1)
    if q != 0:  # N is composite
        ret = ret * int(q - 1)

    return ret

def calculate_d(e: int, phi: int) -> int:
    '''
    d = e^-1 (mod phi)
    '''
    return mu.inv_mod(e, phi)

def decrypt(c: int, d: int, n: int) -> int:
    '''
    m = c^d mod n
    '''
    return mu.pow_mod(c,d,n)

def encrypt(m: int, e: int, n: int) -> int:
    '''
    c = m^e mod n
    '''
    return mu.pow_mod(m,e,n)
