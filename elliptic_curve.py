import math_util
from ui_logger import ui_log,render_logs

class EC_Point:
    x =  int(0)
    y = int(0)

    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def print(self):
        text = "({0},{1})"
        print(text.format(self.x, self.y))


class Elliptic_Curve:
    '''
    Y^2 = X^3 + AX + B  over Z(F)
    '''
    A = int(0)
    B = int(0)
    F = int(0)

    ZERO = EC_Point(0,0)
    delta = int(0)

    def __init__(self, a, b, f):
        a = int(a)
        b = int(b)
        f = int(f)

        self.A = a
        self.B = b
        self.F = f

        self.ZERO = EC_Point(f, f)
        self.delta = (4*a*a*a) % f + (27*b*b) % f
        self.delta = (self.delta % f)

    def set_zero(self, p):
        '''
        Set a point p to ZERO on curve
        '''
        p.x = self.F
        p.y = self.F

    def calculate_lamda(self, p1, p2):
        '''
        lamda is needed for addition algorithm
        '''
        t = int(0)
        inv = int(0)

        if ( p1 == p2 ):
            t = (3 * p1.x * p1.x + self.A) % self.F
            inv = math_util.inv_mod(2*p1.y, self.F)

        else:
            t = math_util.sub_mod(p2.y, p1.y, self.F)
            inv = math_util.inv_mod( math_util.sub_mod(p2.x, p1.x, self.F), self.F)

        t = t * inv % self.F
        return t

    def add(self, p1, p2):
        '''
        Add two points p1 + p2 on curve
        '''
        # handle special cases
        if p1 == self.ZERO:
            return p2

        if p2 == self.ZERO:
            return p1

        if ( p1.x == p2.x and p1.y != p2.y):
            return self.ZERO

        # calculate lamda
        t = self.calculate_lamda(p1, p2)
        
        # final calculation
        ret = EC_Point(0,0)
        ret.x = math_util.sub_mod(t*t, p1.x + p2.x, self.F)
        ret.y = (t * math_util.sub_mod(p1.x, ret.x, self.F) ) % self.F
        ret.y = ( math_util.sub_mod(ret.y, p1.y, self.F) ) % self.F

        return ret

    def double_add(self, p, n):
        n = int(n)
        r = EC_Point(self.F, self.F)

        while n != 0:
            if n % 2 == 1:
                r = self.add(r, p)
            p = self.add(p, p)
            n = n // 2

        return r

    def check_reciprocal(self, p, q):
        '''
        This function is used in Lenstra factorization algorithm
        During addition algorithm, we need to calculate inverse modulo
        This is no problem if F is prime.
        But if F is composite, we need to check their gcd
        if it is 1 => return 1 (which means we can do inverse modulo)
        else return 0
        '''
        d = int(0)
        ret = int(0)

        if p == q:
            deno = int(2*p.y)
        else:
            deno = math_util.sub_mod(q.x, p.x, self.F)

        d = math_util.gcd(deno, self.F)
        txt = "gcd({0},{1}) = {2}"
        ui_log(txt.format(deno,self.F,d))
        
        if d == 1:
            ret = int(1)
        else:
            ret = int(0)

        return ret, d
