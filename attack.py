import sys

import math_util as mu
import back_end as be


if __name__ == "__main__":
    choice = int(sys.argv[1])
    fname = sys.argv[2]
    f = open(fname, 'r')

    if choice == 1:
        #public parameter
        n = int(f.readline())
        e = int(f.readline())

        print("Public params: ")
        print("n = {0}".format(n))
        print("e = {0}".format(e))
  
        # bob encrypt message m
        m = int(f.readline())
        c = be.encrypt(m, e, n)

        print("\nBob encrypts message: ")
        print("m = {0}".format(m))
        print("c = {0}".format(c))

        # eve calculate cc = k^e * c
        k = int(f.readline())     # choose k that gcd(k,n) = 1
        cc = ( mu.pow_mod(k,e,n) * c ) % n

        print("\nEve encrypt modfied message:")
        print("k = {0}".format(k))
        print("cc = {0}".format(cc))

        # alice decrypt mm = cc^d mod n
        d = int(f.readline())
        mm = be.decrypt(cc, d, n)

        print("\nAlice decrypt modifed message:")
        print("d = {0}".format(d))
        print("mm = {0}".format(mm))

        # eve calculate mmm = mm * k^-1 mod n
        mmm = (mm * mu.inv_mod(k, n) ) % n
        print("\nEve get original message:")
        print("mmm = {0}".format(mmm))

        if mmm == m:
            print("equal")
        else:
            print("different")
    else:
        # public param
        n = int(f.readline())
        e1 = int(f.readline())
        e2 = int(f.readline())
        print("n = {0}".format(n))
        print("e1 = {0}".format(e1))
        print("e2 = {0}".format(e2))

        # Eve catches c1, c2
        print("\nEve catches c1, c2")
        c1 = int(f.readline())
        c2 = int(f.readline())
        print("c1 = {0}".format(c1))
        print("c2 = {0}".format(c2))

        # Eve calculate e1.u + e2.v = gcd(e1, e2)
        u,v,g = mu.ext_euclid(e1, e2)
        print("\nEve use extended Euclidean:")
        print("{0}*{1} + {2}*{3} = {4}".format(e1,u,e2,v,g))
        if g == 1:
            #calculate m
            if u < 0:
                c1 = mu.inv_mod(c1, n)
                u = -u
            if v < 0:
                c2 = mu.inv_mod(c2, n)
                v = -v

            x1 = mu.pow_mod(c1,u,n)
            x2 = mu.pow_mod(c2,v,n)
            print("c1^u = {0}".format(x1))
            print("c2^v = {0}".format(x2))
            m = x1 * x2 % n
            print("m = {0}".format(m))

            cc1 = mu.pow_mod(m, e1, n)
            cc2 = mu.pow_mod(m, e2, n)
            print("cc1 = {0}".format(cc1))
            print("cc2 = {0}".format(cc2))

    f.close()
