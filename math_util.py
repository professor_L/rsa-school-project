import numpy as np
import random

import elliptic_curve as ec
from ui_logger import ui_log,render_logs

def gcd(a: int, b: int) -> int:
    '''
    Find greatest common divisor of (a,b)
    '''

    if a == 0 or b == 0:
        return a + b

    if a < b:
        a,b = b,a

    r = b
    while r != 0:
        r = a % b
        a = b
        b = r

    return a

def ext_euclid(a: int, b: int):
    '''
    Find u, v that satisfies
    au + bv = gcd(a,b)
    return u, v, gcd(a,b)
    '''
    if a == 0:
        return 0, 1, b
    if b == 0:
        return 1, 0, a

    flag = 0
    if a < b:
        a,b = b,a
        flag = 1

    u = 1
    g = a
    x = 0
    y = b
    while y != 0:
        # step 1: perform division
        q = g // y
        t = g % y
    
        # step 2: setup next round
        s = u - q*x
        u = x
        g = y
        x = s
        y = t

    v = (g - a*u)//b
    if flag == 1:
        u,v = v,u

    return u,v,g


def sub_mod(a: int, b: int, n: int) -> int:
    '''
    Perform: a - b in Fn
    '''
    a = a % n
    b = b % n

    if a < b:
        a = a + n
    return a - b

def pow_mod(g: int, A: int, n: int) -> int:
    '''
    Compute: b = g^A mod N
    '''


    a = g
    b = 1
    while A > 0:
        if A % 2 == 1:
            b = (b*a) % n
        a = (a*a) % n
        A = A // 2

    return b

def ext_euclid_v2(a: int, b: int):
    '''
    This is an extended Euclid algorithm
    Find u,v: |a*u - b*v| = 1
    This function is used for inverse modulo
    '''
    flag = 0
    if a < b:
        a,b = b,a
        flag = 1

    p = 0
    p1 = 1
    p2 = 0

    q = 0
    q1 = 0
    q2 = 1
    qt = 0

    r = b

    while r != 0:
        qt = a // b
        r = a % b

        p = qt*p1 + p2
        p2 = p1
        p1 = p

        q = qt*q1 + q2
        q2 = q1
        q1 = q

        a = b
        b = r

    u = q2
    v = p2

    if flag == 1:
        u,v = v,u

    return u,v

def inv_mod(a: int, n: int) -> int:
    '''
    Inverse modulo
    Find x = a^-1 mod n
    '''
    a = a
    n = n

    u,v = ext_euclid_v2(a,n)
    if a*u < n*v:
        u = n - u
    return u

def setup_curve(n: int):
    '''
    Set up a curve to factor N
    Randomly choose a Point P, a number A
    Calculate number B = P.y * P.y - P.x * P.x *P.x - P.x * A
    If delta of curve is not 0 => use this curve and point P
    '''
    n = n

    ret = ec.Elliptic_Curve(0, 0, n)

    while ret.delta == 0:
        x = int(np.random.randint(n))
        y = int(np.random.randint(n))
        a = int(np.random.randint(n))
        b = int( sub_mod( y*y, x*x*x + x*a, n) )
        ret = ec.Elliptic_Curve(a, b, n)
        p = ec.EC_Point(x,y)

    txt = "Elliptic Curve: (A,B,F) = ({0},{1},{2})"
    print(txt.format(a, b, n))
    ui_log(txt.format(a, b, n))
    txt = "Point P({0},{1})"
    print(txt.format(x,y))
    ui_log(txt.format(x,y))

    return ret,p

def factor_lenstra(n: int) -> int:
    '''
    Use elliptic curve to factor n.
    Return 2 numbers:
    - Result: 1 succeed, 0 fail
    - Factor q: a factor of n. If result is 0, q = 0
    There are 2 reasons for failure:
        + n is a prime number, so can not factor
        + n is composite, but we are unlucky
    '''

    e,p = setup_curve(n)

    bound = 3*n // 4 + 3

    for j in range(2,bound):

        r = ec.EC_Point(n, n)
        k = j

        q = 0
        ret = 0

        txt = "Calculating R = {0}!*P"
        ui_log(txt.format(j))
        while k != 0:
            if k % 2 != 0:
                # check if inverse modulo can be performed
                ret,q = e.check_reciprocal(r, p)
                if ret == 0:
                    if q < n:
                        return q
                    else:
                        return 1

                r = e.add(r, p)

            # check if inverse modulo can be performed
            ret, q = e.check_reciprocal(r, p)
            if ret == 0:
                if q < n:
                    return q
                else:
                    return 1

            p = e.add(p, p)
            k = k // 2

        p = ec.EC_Point(r.x, r.y)

    return 1

def miller_rabin_test(n: int, a: int) -> bool:
    '''
    '''
    if (n % 2) == 0:
        return 1

    if (gcd(n, a) != 1):
        return 1

    # write n - 1 = 2^k * q
    q = n - 1
    k = 0
    while (q % 2 == 0):
        q = q // 2
        k += 1

    # calculate a = a^q mod n and test
    a = pow_mod(a, q, n)
    if a == 1:
        return 0
    for i in range(k):
        if a == n - 1:
            return 0
        a = (a * a) % n
    return 1

def is_prime_brute_force(n: int) -> bool:
    ui_log("Running check prime brute force ....")
    if n == 1 or n % 2 == 0:
        return False

    i = 3
    while i*i <= n:
        if n % i == 0:
            return False
        i += 2
    return True

def is_prime_miller_rabin(n: int) -> bool:
    '''
    '''
    ui_log("Running Miller Rabin test ....")
    num_witness = (n - 2) * 3 // 4 + 1
    num_witness = min(200, num_witness)
    print("Length of witness: ", num_witness)
    txt = "Number of witnesses: {0}"
    ui_log(txt.format(num_witness))

    # fill array with random values
    a = random.sample(range(2, n-1), num_witness)
    print("List of witness: ", a)
    ui_log("List of witness: ")
    ui_log(a)

    ret = 0
    flag = 0

    # run miller-rabin test with this array
    for i in range(num_witness):
        txt = "Testing a = {0}"
        ui_log(txt.format(a[i]))
        ret = miller_rabin_test(n, a[i])
        if ret == 1:
            txt = "{0} is a witness"
            ui_log(txt.format(a[i]))

            flag = 1
            break
    return 1 - flag

def factor_pollard(n: int) -> int:
    '''
    Return: an integer p > 1 if succeeds, p = 1 if fails
    '''
    ui_log("Running Pollard algorithm ...")

    #defining bound and a values
    bound = 3*n // 4 + 1
    list_a = [2, 3, 5, 7, 11]

    #running algorithm with a
    for a in list_a:
        print("Run pollard p-1 algorithm with a = ", a)
        txt = "Trying a = {0}"
        ui_log(txt.format(a))

        b = a   #for printng purpose
        for j in range(2, bound):
            a = pow_mod(a,j,n)
            txt = "{0}^{1}! = {2} (mod {3})"
            ui_log(txt.format(b,j,a, n))

            d = gcd(a-1,n)
            txt = "gcd({0}-1,{1}) = {2}"
            ui_log(txt.format(a,n,d))
            if 1 < d < n:
                return d
    return 1

def rho_f(x: int, c: int, n: int) -> int:
    '''
    f(x) = x*x + c mod n
    '''
    return (x*x % n + c) % n

def factor_pollard_rho_run(n: int, x: int, c: int) -> int:
    '''
    pollard rho's factorization
    '''
    y = 0
    d = 0
    while x != y:
        x = rho_f(x, c, n)

        # y moves 2 times faster
        y = rho_f(y, c, n)
        y = rho_f(y, c, n)

        d = gcd( sub_mod(y,x,n), n)

        txt = "gcd(|{0} - {1}|,{2}) = {3}"
        print(txt.format(y,x,n,d))
        ui_log(txt.format(y,x,n,d))
        if d != 1:
            if d == n:
                return 0;
            else:
                return d
    return 0


def factor_pollard_rho(n: int) -> int:
    '''
    '''
    count = 5
    while count > 0:
        txt = "{0} tries left"
        print(txt.format(count))
        ui_log(txt.format(count))

        x = random.sample(range(2, n-1), 1)[0]
        c = random.sample(range(2, n-1), 1)[0]

        ret = factor_pollard_rho_run(n, x, c)
        if ret > 0:
            return ret

        count -= 1

    return 0
